use {
    druid::{theme, AppLauncher, WindowDesc},
    structopt::StructOpt,
};

mod data;
mod delegate;
mod lens;
mod view;
mod window_lens;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "json-logs-viewer",
    about = "Visualize a logs file (in json array format)"
)]
struct Opt {
    #[structopt(short, long, parse(from_os_str))]
    file: Option<std::path::PathBuf>,
}

pub fn main() {
    let opt = Opt::from_args();

    let main_window = WindowDesc::new(view::build_ui)
        .title("Json Logs Viewer")
        // .set_window_state(druid::WindowState::MAXIMIZED)
        .window_size((1000.0, 800.0));

    let mut state = data::AppData::new();
    if let Some(file) = opt.file {
        let _ = state.load(&file);
    }

    AppLauncher::with_window(main_window)
        .delegate(delegate::AppDelegate)
        .configure_env(|env, _| {
            use druid::{FontDescriptor, FontFamily, FontStyle, FontWeight};
            env.set(
                theme::UI_FONT,
                FontDescriptor::new(FontFamily::MONOSPACE).with_size(15.0),
            );
            env.set(
                theme::UI_FONT_BOLD,
                FontDescriptor::new(FontFamily::MONOSPACE)
                    .with_weight(FontWeight::BOLD)
                    .with_size(15.0),
            );
            env.set(
                theme::UI_FONT_ITALIC,
                FontDescriptor::new(FontFamily::MONOSPACE)
                    .with_style(FontStyle::Italic)
                    .with_size(15.0),
            );
        })
        .launch(state)
        .expect("Failed to launch application");
}
