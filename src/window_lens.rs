use {
    druid::{
        BoxConstraints, Data, Env, Event, EventCtx, LayoutCtx, LifeCycle, LifeCycleCtx, PaintCtx,
        Size, UpdateCtx, Widget, WidgetId, WindowId,
    },
    std::marker::PhantomData,
};

pub trait WindowLens<T: ?Sized, U: ?Sized> {
    fn with<V, F: FnOnce(&U) -> V>(&self, data: &T, window_id: WindowId, f: F) -> V;
    fn with_mut<V, F: FnOnce(&mut U) -> V>(&self, data: &mut T, window_id: WindowId, f: F) -> V;
}

pub fn with_window_lens<T, U, L, W>(inner: W, lens: L) -> WindowLensWrap<T, U, L, W>
where
    T: Data,
    U: Data,
    L: WindowLens<T, U>,
    W: Widget<U>,
{
    WindowLensWrap {
        inner,
        lens,
        phantom_u: PhantomData,
        phantom_t: PhantomData,
    }
}

pub struct WindowLensWrap<T, U, L, W> {
    inner: W,
    lens: L,
    phantom_u: PhantomData<U>,
    phantom_t: PhantomData<T>,
}

impl<T, U, L, W> Widget<T> for WindowLensWrap<T, U, L, W>
where
    T: Data,
    U: Data,
    L: WindowLens<T, U>,
    W: Widget<U>,
{
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        let inner = &mut self.inner;
        self.lens.with_mut(data, ctx.window_id(), |data| {
            inner.event(ctx, event, data, env)
        })
    }

    fn lifecycle(&mut self, ctx: &mut LifeCycleCtx, event: &LifeCycle, data: &T, env: &Env) {
        let inner = &mut self.inner;
        self.lens.with(data, ctx.window_id(), |data| {
            inner.lifecycle(ctx, event, data, env)
        })
    }

    fn update(&mut self, ctx: &mut UpdateCtx, old_data: &T, data: &T, env: &Env) {
        let inner = &mut self.inner;
        let lens = &self.lens;
        lens.with(old_data, ctx.window_id(), |old_data| {
            lens.with(data, ctx.window_id(), |data| {
                if ctx.has_requested_update() || !old_data.same(data) || ctx.env_changed() {
                    inner.update(ctx, old_data, data, env);
                }
            })
        });
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, bc: &BoxConstraints, data: &T, env: &Env) -> Size {
        let inner = &mut self.inner;
        self.lens.with(data, ctx.window_id(), |data| {
            inner.layout(ctx, bc, data, env)
        })
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &T, env: &Env) {
        let inner = &mut self.inner;
        self.lens
            .with(data, ctx.window_id(), |data| inner.paint(ctx, data, env))
    }

    fn id(&self) -> Option<WidgetId> {
        self.inner.id()
    }
}
