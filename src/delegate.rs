use {
    crate::{data, view},
    druid::{
        commands::OPEN_FILE, Command, DelegateCtx, Env, Handled, Selector, Target, WindowDesc,
        WindowId,
    },
    std::path::Path,
};

pub const ADD_REGEXP_FILTER: Selector<()> = Selector::new("json-viewer.add-regexp-filter");
pub const ADD_COMPARE_FILTER: Selector<()> = Selector::new("json-viewer.add-compare-filter");
pub const REMOVE_FILTER: Selector<data::filter::Uuid> = Selector::new("json-viewer.remove-filter");
pub const APPLY_FILTERS: Selector<()> = Selector::new("json-viewer.apply-filter");

pub struct AppDelegate;

fn open_loading_error_dialog(
    ctx: &mut DelegateCtx,
    data: &mut data::AppData,
    error: data::document::ContentError,
    path: &Path,
) {
    let window_desc = WindowDesc::new(view::error_dialog::build_dialog)
        .title("Loading error")
        .with_min_size((50.0, 50.0));
    let id = window_desc.id;

    let message = match error {
        data::document::ContentError::CouldNotLoadFile => "Error while reading the file",
        data::document::ContentError::CouldNotParseJson => "Invalid json file",
        data::document::ContentError::NotAnArrayOfObject => {
            "The file is not an json array of object"
        }
    };
    let message = format!("Could not open {}:\n{}", path.display(), message);
    data.dialog_messages.push((id, message.into()));

    ctx.new_window(window_desc);
}

impl druid::AppDelegate<data::AppData> for AppDelegate {
    fn command(
        &mut self,
        ctx: &mut DelegateCtx,
        _target: Target,
        cmd: &Command,
        data: &mut data::AppData,
        _env: &Env,
    ) -> Handled {
        if cmd.is(ADD_REGEXP_FILTER) {
            data.add_regexp_filter();
            Handled::Yes
        } else if cmd.is(ADD_COMPARE_FILTER) {
            data.add_compare_filter();
            Handled::Yes
        } else if let Some(uuid) = cmd.get(REMOVE_FILTER) {
            data.remove_filter(uuid);
            Handled::Yes
        } else if cmd.is(APPLY_FILTERS) {
            data.apply_filters();
            Handled::Yes
        } else if let Some(file_info) = cmd.get(OPEN_FILE) {
            if let Err(e) = data.load(file_info.path()) {
                eprintln!("Failed to open {}", file_info.path().display());
                open_loading_error_dialog(ctx, data, e, file_info.path());
            }

            Handled::Yes
        } else {
            Handled::No
        }
    }

    fn window_removed(
        &mut self,
        id: WindowId,
        data: &mut data::AppData,
        _env: &Env,
        _ctx: &mut DelegateCtx,
    ) {
        data.dialog_messages.retain(|p| p.0 != id);
    }
}
