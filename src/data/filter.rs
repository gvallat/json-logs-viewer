use {
    crate::data::document::*,
    druid::{im::Vector, ArcStr, Data, Lens},
    serde_json::Value,
    std::sync::Arc,
};

#[derive(Clone, Data, Lens)]
pub struct FilterRegExp {
    pub column: String,
    pub reg_exp: String,
}

#[derive(Clone, Data, Lens)]
pub struct FilterCompare {
    pub column: String,
    pub greater: bool,
    pub value: f64,
}

#[derive(Clone, Data)]
#[allow(unused)]
pub enum Filter {
    RegExp(FilterRegExp),
    Compare(FilterCompare),
}

#[allow(unused)]
enum FilterImpl {
    RegExp {
        column: usize,
        reg_exp: regex::Regex,
    },
    Compare {
        column: usize,
        greater: bool,
        value: f64,
    },
}

impl FilterImpl {
    fn accept(&self, object: &Vector<(Arc<Option<Value>>, ArcStr)>) -> bool {
        match self {
            FilterImpl::RegExp { column, reg_exp } => reg_exp.is_match(&object[*column].1),
            FilterImpl::Compare { .. } => true,
        }
    }
}

#[derive(Clone, PartialEq, Eq)]
pub struct Uuid(uuid::Uuid);

impl Uuid {
    fn new() -> Self {
        Self(uuid::Uuid::new_v4())
    }

    #[allow(unused)]
    pub fn to_string(&self) -> String {
        self.0.to_string()
    }
}

impl Data for Uuid {
    fn same(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

pub type UuidFilter = (Uuid, Filter);
pub type UuidFilterRegExp = (Uuid, FilterRegExp);
pub type UuidFilterCompare = (Uuid, FilterCompare);

#[derive(Clone, Data, Lens)]
pub struct Filters {
    pub filters: Vector<UuidFilter>,
    // show columns ?
}

fn compile_filters(filters: &Vector<UuidFilter>, columns: &Vector<ArcStr>) -> Vec<FilterImpl> {
    use std::ops::Deref;

    let mut result = Vec::with_capacity(filters.len());
    for filter in filters.iter() {
        match &filter.1 {
            Filter::RegExp(filter) => {
                if filter.reg_exp.is_empty() {
                    continue;
                }

                if let Some((column, _)) = columns
                    .iter()
                    .enumerate()
                    .find(|(_, name)| name.deref().deref() == filter.column.as_str())
                {
                    if let Ok(reg_exp) = regex::Regex::new(&filter.reg_exp) {
                        result.push(FilterImpl::RegExp { column, reg_exp });
                    }
                }
            }
            Filter::Compare(_filter) => {}
        }
    }
    result
}

impl Filters {
    pub fn new() -> Self {
        Self {
            filters: Vector::new(),
        }
    }

    pub fn filter(&self, doc: &Document) -> DisplayDocument {
        let columns = doc.columns.clone();

        let filters = compile_filters(&self.filters, &doc.columns);
        let objects = if filters.is_empty() {
            doc.objects
                .iter()
                .map(|obj| obj.iter().map(|(_, t)| t.clone()).collect())
                .collect()
        } else {
            doc.objects
                .iter()
                .filter(|obj| {
                    for filter in filters.iter() {
                        if !filter.accept(*obj) {
                            return false;
                        }
                    }
                    true
                })
                .map(|obj| obj.iter().map(|(_, t)| t.clone()).collect())
                .collect()
        };

        DisplayDocument { columns, objects }
    }

    pub fn add_regexp_filter(&mut self, first_column: &ArcStr) {
        self.filters.push_back((
            Uuid::new(),
            Filter::RegExp(FilterRegExp {
                column: first_column.to_string(),
                reg_exp: String::default(),
            }),
        ));
    }

    pub fn add_compare_filter(&mut self, first_column: &ArcStr) {
        self.filters.push_back((
            Uuid::new(),
            Filter::Compare(FilterCompare {
                column: first_column.to_string(),
                greater: true,
                value: 0.0,
            }),
        ));
    }

    pub fn remove_filter(&mut self, uuid: &Uuid) {
        self.filters.retain(|f| &f.0 != uuid);
    }
}
