use {
    druid::{im::Vector, ArcStr, Data, Lens},
    serde_json::{Map, Value},
    std::path::Path,
    std::sync::Arc,
};

#[derive(Clone, Default)]
pub struct Document {
    pub columns: Vector<ArcStr>,
    pub objects: Vector<Vector<(Arc<Option<Value>>, ArcStr)>>,
}

#[derive(Clone, Copy, Data, PartialEq, Eq)]
pub enum ContentError {
    CouldNotLoadFile,
    CouldNotParseJson,
    NotAnArrayOfObject,
}

#[derive(Clone, Data, Lens, Default)]
pub struct DisplayDocument {
    pub columns: Vector<ArcStr>,
    pub objects: Vector<Vector<ArcStr>>,
}

impl Document {
    pub fn load<P: AsRef<Path>>(path: P) -> Result<Document, ContentError> {
        let data =
            std::fs::read_to_string(path.as_ref()).map_err(|_| ContentError::CouldNotLoadFile)?;
        let json = serde_json::from_str(&data).map_err(|_| ContentError::CouldNotParseJson)?;

        if let Value::Array(array) = json {
            let objects: Vector<_> = array
                .into_iter()
                .map(|v| match v {
                    Value::Object(map) => Ok(map),
                    _ => Err(()),
                })
                .collect::<Result<Vector<_>, ()>>()
                .map_err(|_| ContentError::NotAnArrayOfObject)?;

            let mut columns: Vector<String> = Vector::new();
            for object in objects.iter() {
                for (name, _) in object.iter() {
                    if !columns.contains(&name) {
                        columns.push_back(name.clone());
                    }
                }
            }

            let columns = columns.into_iter().map(Into::into).collect();
            let objects = objects
                .into_iter()
                .map(|obj| convert_object(obj, &columns))
                .collect();

            Ok(Document { columns, objects })
        } else {
            Err(ContentError::NotAnArrayOfObject)
        }
    }
}

fn value_to_string(value: &Value) -> String {
    let text = match value {
        Value::Null => String::default(),
        Value::Number(val) => format!("{}", val),
        Value::Bool(val) => {
            if *val {
                "true".to_string()
            } else {
                "false".to_string()
            }
        }
        Value::String(val) => val.clone(),
        Value::Array(_) => serde_json::to_string_pretty(value).unwrap_or_default(),
        Value::Object(_) => serde_json::to_string_pretty(value).unwrap_or_default(),
    };
    textwrap::fill(&text, 120)
}

fn convert_object(
    mut object: Map<String, Value>,
    columns: &Vector<ArcStr>,
) -> Vector<(Arc<Option<Value>>, ArcStr)> {
    use std::ops::Deref;
    let mut new_object = Vector::new();
    for column in columns {
        match object.remove(column.deref()) {
            Some(value) => {
                let text = value_to_string(&value);
                new_object.push_back((Arc::new(Some(value)), text.into()));
            }
            None => {
                new_object.push_back((Arc::new(None), String::default().into()));
            }
        }
    }
    new_object
}
