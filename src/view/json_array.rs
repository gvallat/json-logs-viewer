use {
    crate::data,
    druid::{
        widget::{Flex, Painter, RawLabel, Scroll, SizedBox},
        ArcStr, BoxConstraints, Color, Env, Event, EventCtx, LayoutCtx, LifeCycle, LifeCycleCtx,
        PaintCtx, Point, Rect, RenderContext, Size, UpdateCtx, Widget, WidgetExt, WidgetPod,
    },
    std::cmp::Ordering,
};

pub fn build_view() -> impl Widget<data::AppData> {
    let header = TableHeaderWidget::new();
    Scroll::new(Flex::column().with_child(header)).padding(5.0)
}

fn build_header_label() -> RawLabel<ArcStr> {
    RawLabel::new()
}

fn build_header_background() -> Box<dyn Widget<()>> {
    let background = Painter::new(|ctx, _: &(), _| {
        let bounds = ctx.size().to_rect();
        let color = Color::from_hex_str("#d1d1d1").unwrap();
        ctx.stroke(bounds, &color, 1.0);
    });

    SizedBox::empty().expand().background(background).boxed()
}

fn build_cell_label() -> RawLabel<ArcStr> {
    RawLabel::new()
}

fn build_cell_background() -> Box<dyn Widget<()>> {
    let background = Painter::new(|ctx, _: &(), _| {
        let bounds = ctx.size().to_rect();
        let color = Color::from_hex_str("#323178").unwrap();
        ctx.fill(bounds, &color);
    });

    SizedBox::empty().expand().background(background).boxed()
}

struct TableHeaderWidget {
    header_labels: Vec<WidgetPod<ArcStr, RawLabel<ArcStr>>>,
    header_backgrounds: Vec<WidgetPod<(), Box<dyn Widget<()>>>>,

    cell_labels: Vec<Vec<WidgetPod<ArcStr, RawLabel<ArcStr>>>>,
    cell_backgrounds: Vec<Vec<WidgetPod<(), Box<dyn Widget<()>>>>>,
}

impl TableHeaderWidget {
    fn new() -> Self {
        Self {
            header_labels: Vec::new(),
            header_backgrounds: Vec::new(),
            cell_labels: Vec::new(),
            cell_backgrounds: Vec::new(),
        }
    }

    fn update_child_count(&mut self, data: &data::AppData) -> bool {
        // update columns count
        let new_col_count = data.document.columns.len();
        let new_row_count = data.document.objects.len();
        let row_count = self.cell_labels.get(0).map(|c| c.len()).unwrap_or(0);
        let col_count = self.header_labels.len();
        match col_count.cmp(&new_col_count) {
            Ordering::Greater => {
                self.header_labels.truncate(new_col_count);
                self.header_backgrounds.truncate(new_col_count);
                self.cell_labels.truncate(new_col_count);
                self.cell_backgrounds.truncate(new_col_count);
            }
            Ordering::Less => {
                let needed = new_col_count - col_count;
                self.header_labels.reserve(needed);
                self.header_backgrounds.reserve(needed);
                self.cell_labels.reserve(needed);
                self.cell_backgrounds.reserve(needed);

                for _ in 0..needed {
                    self.header_labels
                        .push(WidgetPod::new(build_header_label()));
                    self.header_backgrounds
                        .push(WidgetPod::new(build_header_background()));

                    let labels = (0..new_row_count)
                        .map(|_| WidgetPod::new(build_cell_label()))
                        .collect();
                    self.cell_labels.push(labels);

                    let backgrounds = (0..new_row_count)
                        .map(|_| WidgetPod::new(build_cell_background()))
                        .collect();
                    self.cell_backgrounds.push(backgrounds);
                }
            }
            Ordering::Equal => (),
        }

        // update rows count
        match row_count.cmp(&new_row_count) {
            Ordering::Greater => {
                self.cell_labels
                    .iter_mut()
                    .for_each(|col| col.truncate(new_row_count));
                self.cell_backgrounds
                    .iter_mut()
                    .for_each(|col| col.truncate(new_row_count));
            }
            Ordering::Less => {
                self.cell_labels.iter_mut().for_each(|col| {
                    let needed = new_row_count - col.len();
                    col.reserve(needed);
                    for _ in 0..needed {
                        col.push(WidgetPod::new(build_cell_label()));
                    }
                });

                self.cell_backgrounds.iter_mut().for_each(|col| {
                    let needed = new_row_count - col.len();
                    col.reserve(needed);
                    for _ in 0..needed {
                        col.push(WidgetPod::new(build_cell_background()));
                    }
                });
            }
            Ordering::Equal => (),
        }

        new_col_count != col_count || new_row_count != row_count
    }
}

impl Widget<data::AppData> for TableHeaderWidget {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut data::AppData, env: &Env) {
        for background in self.header_backgrounds.iter_mut() {
            background.event(ctx, event, &mut (), env);
        }
        for column in self.cell_backgrounds.iter_mut() {
            for background in column.iter_mut() {
                background.event(ctx, event, &mut (), env);
            }
        }

        for (header, data) in self
            .header_labels
            .iter_mut()
            .zip(data.document.columns.iter_mut())
        {
            header.event(ctx, event, data, env);
        }

        for (i, column) in self.cell_labels.iter_mut().enumerate() {
            for (cell, object) in column.iter_mut().zip(data.document.objects.iter_mut()) {
                let data = &mut object[i];
                cell.event(ctx, event, data, env);
            }
        }
    }

    fn lifecycle(
        &mut self,
        ctx: &mut LifeCycleCtx,
        event: &LifeCycle,
        data: &data::AppData,
        env: &Env,
    ) {
        if let LifeCycle::WidgetAdded = event {
            if self.update_child_count(data) {
                ctx.children_changed();
            }
        }

        for background in self.header_backgrounds.iter_mut() {
            background.lifecycle(ctx, event, &(), env);
        }
        for column in self.cell_backgrounds.iter_mut() {
            for background in column.iter_mut() {
                background.lifecycle(ctx, event, &(), env);
            }
        }

        for (header, data) in self
            .header_labels
            .iter_mut()
            .zip(data.document.columns.iter())
        {
            header.lifecycle(ctx, event, data, env);
        }

        for (i, column) in self.cell_labels.iter_mut().enumerate() {
            for (cell, object) in column.iter_mut().zip(data.document.objects.iter()) {
                let data = &object[i];
                cell.lifecycle(ctx, event, data, env);
            }
        }
    }

    fn update(
        &mut self,
        ctx: &mut UpdateCtx,
        _old_data: &data::AppData,
        data: &data::AppData,
        env: &Env,
    ) {
        if self.update_child_count(data) {
            ctx.children_changed();

            for background in self.header_backgrounds.iter_mut() {
                if background.is_initialized() {
                    if background.is_initialized() {
                        background.update(ctx, &(), env);
                    }
                }
            }
            for column in self.cell_backgrounds.iter_mut() {
                for background in column.iter_mut() {
                    if background.is_initialized() {
                        background.update(ctx, &(), env);
                    }
                }
            }

            for (header, data) in self
                .header_labels
                .iter_mut()
                .zip(data.document.columns.iter())
            {
                if header.is_initialized() {
                    header.update(ctx, data, env);
                }
            }

            for (i, column) in self.cell_labels.iter_mut().enumerate() {
                for (cell, object) in column.iter_mut().zip(data.document.objects.iter()) {
                    if cell.is_initialized() {
                        let data = &object[i];
                        cell.update(ctx, data, env);
                    }
                }
            }
        } else {
            for background in self.header_backgrounds.iter_mut() {
                background.update(ctx, &(), env);
            }
            for column in self.cell_backgrounds.iter_mut() {
                for background in column.iter_mut() {
                    background.update(ctx, &(), env);
                }
            }

            for (header, data) in self
                .header_labels
                .iter_mut()
                .zip(data.document.columns.iter())
            {
                header.update(ctx, data, env);
            }

            for (i, column) in self.cell_labels.iter_mut().enumerate() {
                for (cell, object) in column.iter_mut().zip(data.document.objects.iter()) {
                    let data = &object[i];
                    cell.update(ctx, data, env);
                }
            }
        }
    }

    fn layout(
        &mut self,
        ctx: &mut LayoutCtx,
        bc: &BoxConstraints,
        data: &data::AppData,
        env: &Env,
    ) -> Size {
        let child_bc =
            BoxConstraints::new(Size::new(5.0, 5.0), Size::new(f64::INFINITY, f64::INFINITY));
        let header_sizes: Vec<_> = self
            .header_labels
            .iter_mut()
            .zip(data.document.columns.iter())
            .map(|(label, text)| label.layout(ctx, &child_bc, text, env))
            .collect();

        let cell_sizes: Vec<Vec<_>> = self
            .cell_labels
            .iter_mut()
            .enumerate()
            .map(|(i, column)| {
                column
                    .iter_mut()
                    .zip(data.document.objects.iter())
                    .map(|(cell, object)| {
                        let data = &object[i];
                        cell.layout(ctx, &child_bc, data, env)
                    })
                    .collect()
            })
            .collect();

        let inner_cell_widths: Vec<_> = header_sizes
            .iter()
            .zip(cell_sizes.iter())
            .map(|(header, column)| {
                let width = column.iter().fold(0.0 as f64, |f, s| f.max(s.width));
                header.width.max(width)
            })
            .collect();

        let mut paint_rect = Rect::ZERO;
        let padding_x = 10.0;
        let padding_y = 5.0;
        let spacing_x = 2.0;
        let spacing_y = 5.0;

        let mut origin_x = 0.0;
        let header_height =
            padding_y * 2.0 + header_sizes.iter().fold(0.0 as f64, |f, s| f.max(s.height));
        for ((((column_width, inner_size), label), background), column_name) in inner_cell_widths
            .iter()
            .zip(header_sizes.iter())
            .zip(self.header_labels.iter_mut())
            .zip(self.header_backgrounds.iter_mut())
            .zip(data.document.columns.iter())
        {
            let width = column_width + 2.0 * padding_x;
            let size = Size::new(width, header_height);
            let bc = BoxConstraints::new(size, size);
            background.layout(ctx, &bc, &(), env);
            background.set_origin(ctx, &(), env, Point::new(origin_x, 0.0));
            paint_rect = paint_rect.union(background.paint_rect());

            let x = origin_x + (width - inner_size.width) / 2.0;
            let y = (header_height - inner_size.height) / 2.0;
            label.set_origin(ctx, column_name, env, Point::new(x, y));

            origin_x += width + spacing_x;
        }
        let width = origin_x - spacing_x;
        let mut origin_y = header_height + spacing_y;

        let row_heights: Vec<_> = (0..data.document.objects.len())
            .map(|row_index| {
                cell_sizes
                    .iter()
                    .map(|sizes| sizes[row_index].height + 2.0 * padding_y)
                    .fold(0.0 as f64, |f, h| f.max(h))
            })
            .collect();

        origin_x = 0.0;
        for (((column_index, column_width), column_labels), column_background) in inner_cell_widths
            .iter()
            .enumerate()
            .zip(self.cell_labels.iter_mut())
            .zip(self.cell_backgrounds.iter_mut())
        {
            origin_y = header_height + spacing_y;
            let width = column_width + 2.0 * padding_x;

            for (((label, background), row_height), object) in column_labels
                .iter_mut()
                .zip(column_background.iter_mut())
                .zip(row_heights.iter())
                .zip(data.document.objects.iter())
            {
                let size = Size::new(width, *row_height);
                let bc = BoxConstraints::new(size, size);
                background.layout(ctx, &bc, &(), env);
                background.set_origin(ctx, &(), env, Point::new(origin_x, origin_y));
                paint_rect = paint_rect.union(background.paint_rect());

                let x = origin_x + padding_x;
                let y = origin_y + padding_y;
                let data = &object[column_index];
                label.set_origin(ctx, data, env, Point::new(x, y));
                origin_y += row_height + spacing_y;
            }

            origin_x += width + spacing_x;
        }
        let height = origin_y - spacing_y;

        let my_size = bc.constrain(Size::new(width, height));
        let insets = paint_rect - my_size.to_rect();
        ctx.set_paint_insets(insets);
        my_size
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &data::AppData, env: &Env) {
        for background in self.header_backgrounds.iter_mut() {
            background.paint(ctx, &(), env);
        }
        for (header, data) in self
            .header_labels
            .iter_mut()
            .zip(data.document.columns.iter())
        {
            header.paint(ctx, data, env);
        }

        for column in self.cell_backgrounds.iter_mut() {
            for background in column.iter_mut() {
                background.paint(ctx, &(), env);
            }
        }
        for (i, column) in self.cell_labels.iter_mut().enumerate() {
            for (cell, object) in column.iter_mut().zip(data.document.objects.iter()) {
                let data = &object[i];
                cell.paint(ctx, data, env);
            }
        }
    }
}
