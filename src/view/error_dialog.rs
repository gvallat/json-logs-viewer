use {
    crate::{
        data,
        window_lens::{with_window_lens, WindowLens},
    },
    druid::{widget::RawLabel, ArcStr, Widget, WidgetExt, WindowId},
};

struct ErrorMessageLens;

impl WindowLens<data::AppData, ArcStr> for ErrorMessageLens {
    fn with<V, F: FnOnce(&ArcStr) -> V>(
        &self,
        data: &data::AppData,
        window_id: WindowId,
        f: F,
    ) -> V {
        let msg = data
            .dialog_messages
            .iter()
            .find(|p| p.0 == window_id)
            .map(|p| p.1.clone())
            .unwrap_or_else(|| String::default().into());
        f(&msg)
    }

    fn with_mut<V, F: FnOnce(&mut ArcStr) -> V>(
        &self,
        data: &mut data::AppData,
        window_id: WindowId,
        f: F,
    ) -> V {
        let mut msg = data
            .dialog_messages
            .iter()
            .find(|p| p.0 == window_id)
            .map(|p| p.1.clone())
            .unwrap_or_else(|| String::default().into());

        let result = f(&mut msg);

        if let Some(p) = data.dialog_messages.iter_mut().find(|p| p.0 == window_id) {
            p.1 = msg;
        }
        result
    }
}

pub fn build_dialog() -> impl Widget<data::AppData> {
    let label = RawLabel::new().padding(10.);
    with_window_lens(label, ErrorMessageLens)
}
