use {
    druid::{ArcStr, Data, Lens, WindowId},
    std::path::Path,
};

pub mod document;
pub mod filter;

use document::{ContentError, DisplayDocument, Document};
use filter::Filters;

#[derive(Clone, Data, Lens)]
pub struct AppData {
    #[data(same_fn = "PartialEq::eq")]
    pub dialog_messages: Vec<(WindowId, ArcStr)>,

    pub loaded: bool,
    #[data(ignore)]
    pub source_document: Document,
    pub document: DisplayDocument,

    pub filters: Filters,
}

impl AppData {
    pub fn new() -> Self {
        Self {
            dialog_messages: Vec::new(),
            loaded: false,
            source_document: Document::default(),
            document: DisplayDocument::default(),
            filters: Filters::new(),
        }
    }

    pub fn load<T: AsRef<Path>>(&mut self, path: T) -> Result<(), ContentError> {
        match Document::load(path) {
            Ok(doc) => {
                self.source_document = doc;
                self.document = self.filters.filter(&self.source_document);
                self.loaded = true;
                Ok(())
            }
            Err(e) => {
                self.close();
                Err(e)
            }
        }
    }

    #[allow(unused)]
    pub fn close(&mut self) {
        self.source_document = Document::default();
        self.document = DisplayDocument::default();
        self.loaded = false;
    }

    pub fn apply_filters(&mut self) {
        self.document = self.filters.filter(&self.source_document);
    }

    pub fn add_regexp_filter(&mut self) {
        if let Some(column) = self.document.columns.get(0) {
            self.filters.add_regexp_filter(column);
        }
    }

    pub fn add_compare_filter(&mut self) {
        if let Some(column) = self.document.columns.get(0) {
            self.filters.add_compare_filter(column);
        }
    }

    pub fn remove_filter(&mut self, uuid: &filter::Uuid) {
        self.filters.remove_filter(uuid);
    }
}
