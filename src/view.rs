use {
    crate::{data, delegate::*, lens::*},
    druid::{
        widget::{
            Button, CrossAxisAlignment, Either, Flex, Label, List, MainAxisAlignment, TextBox,
            ViewSwitcher,
        },
        Color, Command, FileDialogOptions, FileSpec, Target, Widget, WidgetExt,
    },
};

pub mod error_dialog;
mod json_array;

fn build_empty_ui() -> impl Widget<data::AppData> {
    Flex::column()
        .cross_axis_alignment(CrossAxisAlignment::Center)
        .main_axis_alignment(MainAxisAlignment::Center)
        .with_child(Button::new("Open file").on_click(|ctx, _, _| {
            let options = FileDialogOptions::new()
                .allowed_types(vec![FileSpec::new("Json", &["json"])])
                .name_label("Content")
                .title("Choose a json file to open")
                .button_text("Open");
            ctx.submit_command(Command::new(
                druid::commands::SHOW_OPEN_PANEL,
                options,
                Target::Auto,
            ))
        }))
        .lens(UnitLens)
}

fn build_filter_regexp_row() -> impl Widget<data::filter::UuidFilterRegExp> {
    // TODO validate column name
    // TODO validate regexp
    Flex::row()
        .must_fill_main_axis(true)
        .with_child(Label::new("Column:"))
        .with_default_spacer()
        .with_flex_child(
            TextBox::new()
                .lens(data::filter::FilterRegExp::column)
                .lens(DropUuidLens)
                .expand_width(),
            1.0,
        )
        .with_default_spacer()
        .with_child(Label::new("Regexp:"))
        .with_default_spacer()
        .with_flex_child(
            TextBox::new()
                .lens(data::filter::FilterRegExp::reg_exp)
                .lens(DropUuidLens)
                .expand_width(),
            3.0,
        )
        .with_default_spacer()
        .with_child(
            Button::new("X").on_click(|ctx, d: &mut data::filter::UuidFilterRegExp, _| {
                ctx.submit_command(REMOVE_FILTER.with(d.0.clone()))
            }),
        )
        .expand_width()
}

fn build_filter_compare_row() -> impl Widget<data::filter::UuidFilterCompare> {
    Flex::row()
        .must_fill_main_axis(true)
        .with_child(Label::new("compare"))
        .with_flex_spacer(1.0)
        .with_child(
            Button::new("X").on_click(|ctx, d: &mut data::filter::UuidFilterCompare, _| {
                ctx.submit_command(REMOVE_FILTER.with(d.0.clone()))
            }),
        )
        .expand_width()
}

fn build_filter_row() -> impl Widget<data::filter::UuidFilter> {
    ViewSwitcher::new(
        |d: &data::filter::UuidFilter, _| match &d.1 {
            data::filter::Filter::RegExp(_) => 0,
            data::filter::Filter::Compare(_) => 1,
        },
        |v: &u8, _: &data::filter::UuidFilter, _| match *v {
            0 => build_filter_regexp_row().lens(FilterRegExpLens).boxed(),
            1 => build_filter_compare_row().lens(FilterCompareLens).boxed(),
            _ => unreachable!(),
        },
    )
}

fn build_filters_view() -> impl Widget<data::AppData> {
    let header = Flex::row()
        .must_fill_main_axis(true)
        .with_child(Label::new("Filters"))
        .with_flex_spacer(1.0)
        .with_child(
            Button::new("Add regexp").on_click(|ctx, _, _| ctx.submit_command(ADD_REGEXP_FILTER)),
        )
        .with_default_spacer()
        .with_child(
            Button::new("Add compare").on_click(|ctx, _, _| ctx.submit_command(ADD_COMPARE_FILTER)),
        )
        .with_default_spacer()
        .with_child(Button::new("Apply").on_click(|ctx, _, _| ctx.submit_command(APPLY_FILTERS)))
        .expand_width();

    let list = List::new(build_filter_row)
        .lens(data::filter::Filters::filters)
        .lens(data::AppData::filters);

    Flex::column()
        .with_child(header)
        .with_child(list)
        .expand_width()
        .padding(10.)
        .border(Color::from_hex_str("#d1d1d1").unwrap(), 2.0)
        .rounded(5.0)
        .padding(10.)
}

fn build_json_viewer() -> impl Widget<data::AppData> {
    Flex::column()
        .must_fill_main_axis(true)
        .with_child(build_filters_view())
        .with_default_spacer()
        .with_flex_child(json_array::build_view(), 1.0)
}

pub fn build_ui() -> impl Widget<data::AppData> {
    Either::new(
        |d: &data::AppData, _| d.loaded,
        build_json_viewer(),
        build_empty_ui(),
    )
}
