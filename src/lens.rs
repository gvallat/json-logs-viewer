use {
    crate::data::filter::{Filter, Uuid, UuidFilter, UuidFilterCompare, UuidFilterRegExp},
    druid::Lens,
};

pub struct UnitLens;

impl<T: ?Sized> Lens<T, ()> for UnitLens {
    fn with<V, F: FnOnce(&()) -> V>(&self, _data: &T, f: F) -> V {
        let unit = ();
        f(&unit)
    }

    fn with_mut<V, F: FnOnce(&mut ()) -> V>(&self, _data: &mut T, f: F) -> V {
        let mut unit = ();
        f(&mut unit)
    }
}

pub struct FilterRegExpLens;

impl Lens<UuidFilter, UuidFilterRegExp> for FilterRegExpLens {
    fn with<V, F: FnOnce(&UuidFilterRegExp) -> V>(&self, data: &UuidFilter, f: F) -> V {
        match &data.1 {
            Filter::RegExp(filter) => {
                let lens_data = (data.0.clone(), filter.clone());
                f(&lens_data)
            }
            _ => unreachable!("Filter should be Filter::RegExp"),
        }
    }

    fn with_mut<V, F: FnOnce(&mut UuidFilterRegExp) -> V>(&self, data: &mut UuidFilter, f: F) -> V {
        match &mut data.1 {
            Filter::RegExp(filter) => {
                let mut lens_data = (data.0.clone(), filter.clone());
                let result = f(&mut lens_data);
                data.0 = lens_data.0;
                *filter = lens_data.1;
                result
            }
            _ => unreachable!("Filter should be Filter::RegExp"),
        }
    }
}

pub struct FilterCompareLens;

impl Lens<UuidFilter, UuidFilterCompare> for FilterCompareLens {
    fn with<V, F: FnOnce(&UuidFilterCompare) -> V>(&self, data: &UuidFilter, f: F) -> V {
        match &data.1 {
            Filter::Compare(filter) => {
                let lens_data = (data.0.clone(), filter.clone());
                f(&lens_data)
            }
            _ => unreachable!("Filter should be Filter::Compare"),
        }
    }

    fn with_mut<V, F: FnOnce(&mut UuidFilterCompare) -> V>(
        &self,
        data: &mut UuidFilter,
        f: F,
    ) -> V {
        match &mut data.1 {
            Filter::Compare(filter) => {
                let mut lens_data = (data.0.clone(), filter.clone());
                let result = f(&mut lens_data);
                data.0 = lens_data.0;
                *filter = lens_data.1;
                result
            }
            _ => unreachable!("Filter should be Filter::Compare"),
        }
    }
}

pub struct DropUuidLens;

impl<T: ?Sized> Lens<(Uuid, T), T> for DropUuidLens {
    fn with<V, F: FnOnce(&T) -> V>(&self, data: &(Uuid, T), f: F) -> V {
        f(&data.1)
    }

    fn with_mut<V, F: FnOnce(&mut T) -> V>(&self, data: &mut (Uuid, T), f: F) -> V {
        f(&mut data.1)
    }
}
